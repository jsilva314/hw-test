package com.test.hwchallenge.model

data class City(val country: String = "",
                val name: String = "",
                val id: Int = 0,
                val idCountry: Int = 0)