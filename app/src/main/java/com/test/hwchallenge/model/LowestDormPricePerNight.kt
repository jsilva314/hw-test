package com.test.hwchallenge.model

data class LowestDormPricePerNight(val currency: String = "",
                                   val value: String = "")