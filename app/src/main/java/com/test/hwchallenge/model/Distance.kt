package com.test.hwchallenge.model

data class Distance(val units: String = "",
                    val value: Double = 0.0)