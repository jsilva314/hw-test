package com.test.hwchallenge.model

data class HighestPricePerNight(val currency: String = "",
                                val value: String = "")