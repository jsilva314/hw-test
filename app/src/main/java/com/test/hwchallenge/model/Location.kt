package com.test.hwchallenge.model

data class Location(val city: City,
                    val region: String? = null)