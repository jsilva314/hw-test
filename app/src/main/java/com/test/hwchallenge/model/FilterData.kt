package com.test.hwchallenge.model

data class FilterData(val highestPricePerNight: HighestPricePerNight,
                      val lowestPricePerNight: LowestPricePerNight)