package com.test.hwchallenge.model

data class ImagesItem(val prefix: String = "",
                      val suffix: String = "")