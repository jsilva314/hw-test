package com.test.hwchallenge.model

data class LowestPrivatePricePerNight(val currency: String = "",
                                      val value: String = "")