package com.test.hwchallenge.model

data class District(val name: String = "",
                    val id: String = "")