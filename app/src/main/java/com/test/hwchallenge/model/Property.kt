package com.test.hwchallenge.model

data class Property(val pagination: Pagination,
                    val filterData: FilterData,
                    val location: Location,
                    val properties: List<Properties>?)