package com.test.hwchallenge.model

data class OverallRating(val numberOfRatings: String = "",
                         val overall: Int = 0)