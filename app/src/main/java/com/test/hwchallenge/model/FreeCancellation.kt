package com.test.hwchallenge.model

data class FreeCancellation(val description: String = "",
                            val label: String = "")