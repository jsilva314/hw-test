package com.test.hwchallenge.model

data class LowestPricePerNight(val currency: String = "",
                               val value: String = "")