package com.test.hwchallenge.model

data class Facilities(val name: String = "",
                      val id: String = "",
                      val facilities: List<Facilities>?)