package com.test.hwchallenge.model

data class Pagination(val next: String = "",
                      val numberOfPages: Int = 0,
                      val prev: String = "",
                      val totalNumberOfItems: Int = 0)