package com.test.hwchallenge.network

import com.test.hwchallenge.AppData
import com.test.hwchallenge.model.Property
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

/**
 * Created by João Paulo Silva on 11/03/2018.
 */
interface HWapi{

    @GET("/dovdtel87/ef6dd1422a86554d22172e5975222f81/raw/ba5b81b567efebc1039a481b7e9712b7cd61ea6c/properties.json")
    fun getProperties():
            Call<Property>

    /**
     * Factory class for convenient creation of the Api Service interface
     */
    companion object Factory {

        fun create(): HWapi {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(AppData.URLBASE)
                    .build()

            return retrofit.create<HWapi>(HWapi::class.java)
        }
    }
}