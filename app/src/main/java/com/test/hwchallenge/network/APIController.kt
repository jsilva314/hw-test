package com.test.hwchallenge.network

import com.test.hwchallenge.model.Properties

/**
 * Created by João Paulo Silva on 11/03/2018.
 */

class APIController {

    private val network = HWapi.create()

    fun getProperties() : List<Properties>?{
        val call = network.getProperties()
        val response = call.execute()
        return when {
            response.isSuccessful -> response.body()!!.properties
            else -> null
        }
    }
}