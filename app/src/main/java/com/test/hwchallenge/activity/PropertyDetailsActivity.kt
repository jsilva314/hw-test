package com.test.hwchallenge.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.test.hwchallenge.AppData
import com.test.hwchallenge.R
import kotlinx.android.synthetic.main.activity_property_details.*

/**
 * Created by João Paulo Silva on 12/03/2018.
 */

class PropertyDetailsActivity : AppCompatActivity() {

    val property = AppData.TempProperty

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_details)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val imageURI = "http://" + (property?.images!![0].prefix + property.images[0].suffix)
        Glide
                .with(this)
                .load(imageURI)
                .into(imageGallery)

        txtPropertyName.text = "${property.name}, ${property?.address1}"
        txtPropertyOverview.text = property?.overview
        txtOverallRating.text = "Overall Rating ${property?.overallRating}"
        txtLowerPrice.text = "Lowest Price per Night : ${toEURO(property?.lowestPricePerNight!!.value).format(2)}€"
    }

    private fun toEURO(value : String) : Double{
        return AppData.CurrencyConvertionRate / value.toDouble()
    }
    private fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)
}