package com.test.hwchallenge.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import co.metalab.asyncawait.async
import com.test.hwchallenge.R
import com.test.hwchallenge.adapter.PropertiesAdapter
import com.test.hwchallenge.model.Properties
import com.test.hwchallenge.network.APIController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val network = APIController()
    private var properties = emptyList<Properties>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        async{
            await { properties = network.getProperties()!! }

            propertyList.adapter = PropertiesAdapter(properties, applicationContext!!)
            val layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
            propertyList.layoutManager = layoutManager
        }


    }
}
