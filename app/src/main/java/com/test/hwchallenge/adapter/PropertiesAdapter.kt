package com.test.hwchallenge.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.test.hwchallenge.AppData
import com.test.hwchallenge.R
import com.test.hwchallenge.activity.PropertyDetailsActivity
import com.test.hwchallenge.model.Properties
import kotlinx.android.synthetic.main.property_item.view.*
/**
 * Created by João Paulo Silva on 11/03/2018.
 */
class PropertiesAdapter(private val properties: List<Properties>?,
                        private val context: Context) : RecyclerView.Adapter<PropertiesAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val property = properties!![position]

        holder?.let {
            it.bindView(property)
        }
        val imageURI = "http://" + (property.images!![0].prefix + property.images[0].suffix)
        Glide
                .with(context)
                .load(imageURI)
                .into(holder.itemView.imgPropertyCover)
        holder.itemView.setOnClickListener {
            AppData.TempProperty = property
            val intent = Intent(context, PropertyDetailsActivity::class.java)
            startActivity(context, intent, null)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.property_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return when (properties) {
            null -> 0
            else -> properties.size
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(p: Properties) {
            itemView.txtName.text = p.name
            itemView.txtCity.text = p.address1
            itemView.txtRating.text = "Rating :" + p.overallRating.overall.toString()
        }

    }

}