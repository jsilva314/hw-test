package com.test.hwchallenge

import com.test.hwchallenge.model.Properties

/**
 * Created by João Paulo Silva on 11/03/2018.
 */
object AppData {

    const val URLBASE = "https://gist.githubusercontent.com/"
    var TempProperty: Properties? = null
    const val CurrencyConvertionRate: Double = 43743.55

}